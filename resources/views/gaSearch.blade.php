<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>News search</title>
</head>
<body>
    <h1>Please choose the date:</h1>
    {!! Form::open(array('url' => 'gaSearch')) !!}
        <p>Start date:</p> <input type = "date" name = "OrderStartDate" placeholder = "2016-01-01" /><br/>
        <p>End date:</p> <input type = "date" name = "OrderEndDate" placeholder = "2016-12-31" /><br/>
        <input type="submit" value="Submit"><br/>
    {!! Form::close() !!}
    <div class="container">
	<div class="content">
		<div class="title">
			<p>Result:</p><br/>
            @if (isset($queryResult))
                @foreach ($queryResult as $key => $value)
				    <p>{{ $key }} => {{ $value  }}</p>
                @endforeach
            @elseif (isset($errorMessage))
                <p>{{ $errorMessage  }}</p>
            @endif
		 </div>
	</div>
    </div>
</body>
</html>

<?php

namespace App\Http\Controllers;
use Log;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Analytics;
use LaravelAnalytics;
use DateTime;
use App\TimeSearch;


class Gatest extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           return view('gaSearch');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = new TimeSearch;
        if ( (int)$request->input('OrderStartDate') > (int)$request->input('OrderEndDate')) {
            return view('gaSearch',['errorMessage' => '你的起始日期需早於結束日期唷！']);
        }
        else {
            $queryResult = $query->queryAllVisitors($request->input('OrderStartDate'), $request->input('OrderEndDate'));
            return view('gaSearch', compact('queryResult'));
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAnalytics;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class TimeSearch extends Model
{
    public function queryAllVisitors($inputStartDate, $inputEndDate)
    {
        // Query all visitors from start date to end date
        $startDate = Carbon::parse($inputStartDate);
        $endDate = Carbon::parse($inputEndDate);
        $metrics = 'ga:users';
        $visitorData = LaravelAnalytics::performQuery(
            $startDate,
            $endDate,
            $metrics,
            $others = [
                'dimensions' => 'ga:date'
            ]
        );

        // Find Maximun and Minimux Visitors Date
        foreach ($visitorData['rows'] as $visitor){
            $dateList[$visitor[0]] = $visitor[1];
        }
       $dateCollection = collect($dateList); 
       $maxDate = array_search($dateCollection->sort()->last(), $dateList); 
       $maxDateNewVisitorNum = $this->queryNewVisitors($maxDate);
       $maxDateReturnVisitorNum = $this->queryReturningVisitors($maxDate);

       $minDate = array_search($dateCollection->sort()->first(), $dateList);
       $minDateNewVisitorNum = $this->queryNewVisitors($minDate);
       $minDateReturnVisitorNum = $this->queryReturningVisitors($minDate);
       $array = [
            'maxDate'   => $maxDate, 
            'maxNew'    => $maxDateNewVisitorNum, 
            'maxReturn' => $maxDateReturnVisitorNum,
            'minDate'   => $minDate, 
            'minNew'    => $minDateNewVisitorNum, 
            'minReturn' => $minDateReturnVisitorNum
        ];
        return collect($array);
    }

    public function queryNewVisitors($inputDate = null)
    {
        // Query
        $startDate = Carbon::parse($inputDate);
        $endDate = Carbon::parse($inputDate);
        $metrics = 'ga:users';
        $newData = LaravelAnalytics::performQuery(
            $startDate,
            $endDate,
            $metrics,
            $others = [
                'dimensions' => 'ga:userType,ga:date',
                'segment' => 'sessions::condition::ga:userType==New Visitor'
            ]
        );
        return $newData['rows'][0][2];
    }

    public function queryReturningVisitors($inputDate = null)    
    {
        // Query
        $startDate = Carbon::parse($inputDate);
        $endDate = Carbon::parse($inputDate);
        $metrics = 'ga:users';
        $returningData = LaravelAnalytics::performQuery(
            $startDate,
            $endDate,
            $metrics,
            $others = [
                'dimensions' => 'ga:userType,ga:date',
                'segment' => 'sessions::condition::ga:userType==Returning Visitor'
            ]
        );
        return $returningData['rows'][0][2];
    }
}

